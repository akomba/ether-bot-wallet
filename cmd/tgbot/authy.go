
package tgbot

import(
	"net/url"
	"fmt"

	"github.com/dcu/go-authy"
	"github.com/ethereum/go-ethereum/cmd/tgbot/secrets"
)

type AuthyApi struct {
	api		*authy.Authy
}

func NewAuthyAPI() *AuthyApi {
	return &AuthyApi{authy.NewAuthyAPI(secrets.GetAuthyToken())}
}

func (self *AuthyApi) RegisterUser(phone string) (string, error) {

	// Get country code and number
	code, number, country, err := GetCountryCodeAndNumber(phone)
	if err != nil {
		fmt.Printf("Authy Register User Error: %s\n", err)
		return "", err
	}

	// Register new user in Authy
	userResponse, err := self.api.RegisterUser(secrets.GetEmail(), code, number, url.Values{})
	if err != nil {
		fmt.Printf("Authy Register User Error: %s\n", err)
		return "", err
	}

	// Check user registration
	if userResponse.Valid() == false {
		fmt.Printf("Authy User is not valid: %s\n", userResponse.Errors)
		return "", fmt.Errorf("User is not valid")
	}

	fmt.Printf("Authy User Registered. Phone: %s Country: %s\n", phone, country)

	return userResponse.ID, nil
}

func (self *AuthyApi) RequestSMS(ID string) error {

	smsResponse, err := self.api.RequestSMS(ID, url.Values{})

	if err != nil {
		fmt.Printf("Authy Request SMS Error: %s\n", err)
		return err
	}

	fmt.Printf("Authy: %s\n", smsResponse.Message)

	return nil
}

func (self *AuthyApi) VerifyToken(ID, token string) bool {

	verify, err := self.api.VerifyToken(ID, token, url.Values{})

	if err != nil {
		fmt.Printf("Authy Verify Token Error: %s\n", err)
		return false
	}

	fmt.Printf("Authy: %s\n", verify.Message)

	return verify.Valid()
}