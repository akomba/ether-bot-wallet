package tgbot

import (
	"bufio"
	"strconv"
	"encoding/csv"
	"fmt"
	"io"
	"os"
)

func GetCountryCodeAndNumber(phone string) (int, string, string, error) {

	if len(phone) < 5 {
		return 0, "", "", fmt.Errorf("Invalid phone")
	}

    file, _ := os.Open("cmd/tgbot/countrycode.csv")
    reader := csv.NewReader(bufio.NewReader(file))

	for {

		row, err := reader.Read()
		if err == io.EOF {
			break
		}

		if err != nil {
			panic(err)
		}

		code := row[0]
		country := row[1]
		codeLen := len(code)

		if phone[:codeLen] == code {
			return toInt(code), phone[codeLen:], country, nil
		}
	}

	return 0, "", "", fmt.Errorf("Invalid phone")
}

func toInt(code string) int {

	i, err := strconv.Atoi(code)
	if err == nil {return i} else {return 0}
}