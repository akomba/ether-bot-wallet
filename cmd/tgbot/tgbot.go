// Telegram bot
package tgbot

import (
	"log"
	"fmt"
	"strings"
	"strconv"
	"encoding/json"

	"github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/ethereum/go-ethereum/eth"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/cmd/tgbot/secrets"
	"github.com/HouzuoGuo/tiedot/db"
)

type Bot struct {
	botDBDir		string
	botDB			*db.DB
	botApi			*tgbotapi.BotAPI
	wallet			*WalletApi
	users			*db.Col
	authy			*AuthyApi
	hello_message	string
	help_message	string
	got_contact		string
}

func New() (*Bot, error) {

	// (Create if not exist) open a database for registered users
	botDBDir := "botWalletDB"
	botDB, err := db.OpenDB(botDBDir)
	if err != nil {
		panic(err)
	}

	users := botDB.Use("Users")

	if users == nil {
		// Create two collection Feeds
		if err := botDB.Create("Users"); err != nil {
			panic(err)
		}

		users = botDB.Use("Users")

		if err := users.Index([]string{"ID"}); err != nil {
			panic(err)
		}
	}

	// Create Bot API
	newBotApi, err := tgbotapi.NewBotAPI(secrets.GetBotToken())
	if err != nil {
		log.Panic(err)
	}

	newBotApi.Debug = false
	log.Printf("Authorized on account %s", newBotApi.Self.UserName)

	authy := NewAuthyAPI()

	hello_message := `Hello,
I am Manager of your EtherWallet
You can share contact for 2FA`

	help_message := `My commands:
/address - see your address
/balance - see your Ether balance
/send <value> <address> - send Ether`

	got_contact := `Got the contact!
I have send a verification token to you.
Please, write it here to approve that is your phone.`

	bot := &Bot{"test",
				botDB,
				newBotApi,
				nil,
				users,
				authy,
				hello_message,
				help_message,
				got_contact}

	return bot, nil
}

func (self *Bot) Start(eth *eth.Ethereum) {

	// Create Ethereum wallet API
	self.wallet = NewWalletApi(eth)

	// Debug print
	self.users.ForEachDoc(func(id int, docContent []byte) (willMoveOn bool) {
		fmt.Println("User: ", string(docContent))
		return true
	})

	// Run hendler in separate thread
	go self.handler()
}

func (self *Bot) handler() {

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates, err := self.botApi.GetUpdatesChan(u)
	if err != nil {
		log.Panic(err)
	}

	for update := range updates {

		log.Printf("id: %d user: %s mes: %s", update.Message.From.ID, update.Message.From.UserName, update.Message.Text)

		self.processMessage(update.Message)
	}
}

func (self *Bot) processMessage(message *tgbotapi.Message) {

	mes := strings.ToLower(message.Text)
	response := tgbotapi.NewMessage(message.Chat.ID, self.help_message)

	if mes == "/start" {

		self.processNewUser(message, &response)

	} else if mes == "/address" {

		self.processAddress(message, &response)

	} else if mes == "/balance" {

		self.processBalance(message, &response)

	} else if len(mes) > 4 && mes[:5] == "/send" {

		self.processSend(message, &response)

	} else if mes == "/help" {

		response.Text = self.help_message

	} else if message.Contact != nil {

		self.processContact(message, &response)

	} else if isAuthyToken(mes) {

		self.processToken(message, &response)

	}

	response.ReplyToMessageID = message.MessageID
	self.botApi.Send(response)
}

func EtherToWei(ether string) string {

	value, err := strconv.ParseFloat(ether, 64)
	if err != nil {
		return "0"
	}
	wei := uint64(value * 1000000000000000000)
	return fmt.Sprintf("%d", wei)
}

func WeiToEther(wei string) string {
	l := int64(len(wei)) - 18
	if l > 0 {
			return wei[:l] + "." + wei[l:]
		} else { return wei }
}

func isAuthyToken(mes string) bool {

	if len(mes) != 7 { return false }

	if _, err := strconv.Atoi(mes); err != nil { return false }

	return true
}

func (self *Bot) getUserIndex(user int) (int, error) {

	var query interface{}
	strID := fmt.Sprintf("%d", user)
	json.Unmarshal([]byte(`[{"eq": "` + strID + `", "in": ["ID"]}]`), &query)
	queryResult := make(map[int]struct{})

	if err := db.EvalQuery(query, self.users, &queryResult); err != nil {
		fmt.Printf("User <%s> not found\n", strID)
	}

	for id := range queryResult {
		return id, nil
	}

	return 0, fmt.Errorf("User is not found")
}

func (self *Bot) getUserData(user int) map[string]interface {} {

	index, err := self.getUserIndex(user)
	if err != nil {
		fmt.Printf("Get Index Error: %s\n", err)
		return nil
	}
	userData, err := self.users.Read(index)
	if err != nil {
		fmt.Printf("Read DB Error: %s\n", err)
		return nil
	}
	return userData
}

func (self *Bot) updateUser(user int, field, data string) {

	userData := self.getUserData(user)

	// Add user data
	userData[field] = data

	// Update user data
	index, err := self.getUserIndex(user)
	if err != nil {
		fmt.Printf("Get Index Error: %s\n", err)
		return
	}
	if err := self.users.Update(index, userData); err != nil {
		fmt.Printf("Update User Error: %s\n", err)
		return
	}
}

func (self *Bot) getUserAddress(id int) string {

	userData := self.getUserData(id)
	return fmt.Sprintf("%s", userData["address"])
}

func (self *Bot) getUserPhone(id int) string {

	userData := self.getUserData(id)
	if userData["phone"] == nil {return ""}
	return fmt.Sprintf("%s", userData["phone"])
}

func (self *Bot) getUserAuthy(id int) string {

	userData := self.getUserData(id)
	if userData["authy"] == nil {return ""}
	return fmt.Sprintf("%s", userData["authy"])
}

func (self *Bot) getUserApprovalPhone(id int) string {

	userData := self.getUserData(id)
	if userData["approval_phone"] == nil {return ""}
	return fmt.Sprintf("%s", userData["approval_phone"])
}

func (self *Bot) getUserApprovalSend(id int) string {

	userData := self.getUserData(id)
	if userData["approval_send"] == nil {return ""}
	return fmt.Sprintf("%s", userData["approval_send"])
}

func (self *Bot) getUserSendRequest(id int) string {

	userData := self.getUserData(id)
	if userData["send"] == nil {return ""}
	return fmt.Sprintf("%s", userData["send"])
}

func (self *Bot) processContact(message *tgbotapi.Message, response *tgbotapi.MessageConfig) {

	fmt.Printf("ID: %d PHONE: %s\n", message.Contact.UserID, message.Contact.PhoneNumber)

	// try to find user in DB by ID
	userData := self.getUserData(message.From.ID)
	if userData == nil {
		fmt.Printf("Process Contact Error: user not found\n")
		return
	}

	// check if user has already approved phone
	if userData["approval_phone"] == "done" {
		fmt.Printf("Process Contact Error: user phone is already approved\n")
		return
	}

	// Check phone number by lenght
	if len(message.Contact.PhoneNumber) < 10 {
		fmt.Printf("Process Contact Error: invalid phone\n")
		return
	}

	// Add phone to DB
	self.updateUser(message.From.ID, "phone", message.Contact.PhoneNumber)

	// Registrate phone in Authy
	authyID, err := self.authy.RegisterUser(message.Contact.PhoneNumber)
	if err != nil {
		fmt.Printf("Process Contact Error: %s\n", err)
		return
	}

	// Add Authy ID to DB
	self.updateUser(message.From.ID, "authy", authyID)

	// Send verification SMS
	if err := self.authy.RequestSMS(authyID); err != nil {
		fmt.Printf("Process Contact Error: %s\n", err)
		return
	}

	// Add approval status of phone verification
	self.updateUser(message.From.ID, "approval_phone", "waiting")

	fmt.Printf("New Contact: %s successfully processed\n", message.Contact.PhoneNumber)

	response.Text = self.got_contact
}

func (self *Bot) processNewUser(message *tgbotapi.Message, response *tgbotapi.MessageConfig) {

	// try to find user in DB by ID
	ID := message.From.ID
	if userData := self.getUserData(ID); userData == nil {

		// Generete new account for new user
		am := self.wallet.Ethereum.AccountManager()
		acc, err := am.NewAccount(fmt.Sprintf("%d", ID))
		if err != nil {
			fmt.Printf("NewAccount Error: %s\n", err)
		}

		// Get address from account
		address := acc.Address.Hex()
		strID := fmt.Sprintf("%d", ID)

		// Write user data to DB
		self.users.Insert(map[string]interface{}{"ID": strID, "address": address})
		fmt.Printf("Registred New User - ID: %d Address: %s\n", ID, address)
	}

	if self.getUserApprovalPhone(message.From.ID) == "done" {
		response.ReplyMarkup = getKeyboardMarkup("commands")
	} else {
		response.ReplyMarkup = getKeyboardMarkup("commands and contact")
	}

	response.Text = self.hello_message
}

func (self *Bot) processAddress(message *tgbotapi.Message, response *tgbotapi.MessageConfig) {

	response.Text = self.getUserAddress(message.From.ID)
}

func (self *Bot) processBalance(message *tgbotapi.Message, response *tgbotapi.MessageConfig) {

	// Get user address
	address := self.getUserAddress(message.From.ID)

	// Get balance for address
	value, err := self.wallet.GetBalance(address)

	if err != nil {
		response.Text = fmt.Sprintf("Error: %s", err)
		return
	}

	response.Text = WeiToEther(value)
}

func (self *Bot) processSend(message *tgbotapi.Message, response *tgbotapi.MessageConfig) {

	// Check phone number
	if self.getUserPhone(message.From.ID) == "" {
		response.Text = "Sorry, I need your contact for 2FA while sending"
		return
	}

	// Check phone aproval
	if self.getUserApprovalPhone(message.From.ID) != "done" {
		response.Text = "Sorry, your phone is not approved."
		return
	}

	// Get user address
	fromStr := self.getUserAddress(message.From.ID)

	// Get value and recipient address from message
	params := strings.Split(message.Text, " ")
	if len(params) < 3 {
		response.Text = "invalid params for send\n/send <value> <address>"
		return
	}
	value := EtherToWei(params[1])
	to_address := params[2]

	if value == "0" {
		response.Text = "invalid value"
		return
	}

	// Get user balance
	balance, err := self.wallet.GetBalance(fromStr)
	if err != nil {
		response.Text = fmt.Sprintf("Error: %s", err)
		return
	}

	// Is enough balance to send value and pay a fee
	if balance < (value + EtherToWei("0.0001")) {
		response.Text = "Your balance is not enough to send the value and pay a fee."
		return
	}

	// Write send reques to DB
	self.updateUser(message.From.ID, "send", value + "*" + to_address)

	// Set send approval status to none
	self.updateUser(message.From.ID, "approval_send", "none")

	// Get Authy ID
	authyID := self.getUserAuthy(message.From.ID)

	// Send verification SMS
	if err := self.authy.RequestSMS(authyID); err != nil {
		fmt.Printf("Process Send Error: %s\n", err)
		response.Text = fmt.Sprintf("Error: %s", err)
		return
	}

	// Set send approval status to waiting
	self.updateUser(message.From.ID, "approval_send", "waiting")

	response.Text = `I've got your send request and have sent a verivication token.
Please, write it here to approve that is your request.`
}

func (self *Bot) processToken(message *tgbotapi.Message, response *tgbotapi.MessageConfig) {

	if self.getUserApprovalPhone(message.From.ID) == "waiting" {

		// Verify token with authy
		if self.authy.VerifyToken(self.getUserAuthy(message.From.ID), message.Text) {

			// Update user approval status
			self.updateUser(message.From.ID, "approval_phone", "done")

			// Set response
			response.ReplyMarkup = getKeyboardMarkup("commands")
			response.Text = "your phone is approved"
			return
		}

		// Update user approval status
		self.updateUser(message.From.ID, "approval_phone", "none")

		// Set response
		response.Text = "invalid token"
		return
	}

	if self.getUserApprovalSend(message.From.ID) == "waiting" {

		// Verify token with authy
		if self.authy.VerifyToken(self.getUserAuthy(message.From.ID), message.Text) {

			// Update user approval status
			self.updateUser(message.From.ID, "approval_send", "done")
		} else {

			// Update user approval status
			self.updateUser(message.From.ID, "approval_send", "none")
			response.Text = "invalid token"
			return
		}

		// Get send request from DB
		request := self.getUserSendRequest(message.From.ID)
		params := strings.Split(request, "*")
		if len(params) < 2 {
			response.Text = "invalid params for send\n/send <value> <address>"
			return
		}
		value := params[0]
		to_address := params[1]

		// Get user address
		fromStr := self.getUserAddress(message.From.ID)

		// Unlock account
		strID := fmt.Sprintf("%d", message.From.ID)
		am := self.wallet.Ethereum.AccountManager()
		err := am.Unlock(common.HexToAddress(fromStr), strID)
		if err != nil {
			fmt.Printf("Account Unlock Error: %s\n", err)
			response.Text = "Account Unlock Error"
			return
		}

		// Send money
		hash, err := self.wallet.SendMoney(fromStr, to_address, value)
		if err != nil {
			fmt.Printf("processSend: %s\n", err)
			response.Text = fmt.Sprintf("Error: %s", err)
			return
		}

		log.Printf("processSend: %s\n", hash)

		// Update user approval status and request
		self.updateUser(message.From.ID, "send", "")
		self.updateUser(message.From.ID, "approval_send", "none")

		// Set response
		response.Text = "done, here is tx hash:\n" + hash
		return
	}

	response.Text = "wtf?"
}

func getKeyboardMarkup(boardType string) tgbotapi.ReplyKeyboardMarkup {

	// Create command buttons
	btnAddress				:= tgbotapi.NewKeyboardButton("/address")
	btnBalance				:= tgbotapi.NewKeyboardButton("/balance")
	btnSend					:= tgbotapi.NewKeyboardButton("/send")
	btnHelp					:= tgbotapi.NewKeyboardButton("/help")

	// Create contact request button
	btnContact				:= tgbotapi.NewKeyboardButtonContact("share my contact")

	// Create button rows
	rowCommand				:= tgbotapi.NewKeyboardButtonRow(btnAddress, btnBalance, btnSend, btnHelp)
	rowContact				:= tgbotapi.NewKeyboardButtonRow(btnContact)

	// Create makrups
	markupCommand			:= tgbotapi.NewReplyKeyboard(rowCommand)
	markupCommandContact	:= tgbotapi.NewReplyKeyboard(rowCommand, rowContact)

	if boardType == "commands" {
		return markupCommand
	}

	if boardType == "commands and contact" {
		return markupCommandContact
	}

	return markupCommandContact
}

func (self *Bot) Stop() {

	log.Printf("Telegram Bot Stopping")

	// Gracefully close database
	if err := self.botDB.Close(); err != nil {
		fmt.Printf("DB Close Error: %s\n", err)
	}

	log.Printf("Telegram Bot Stopped")
}