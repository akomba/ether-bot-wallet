package tgbot

import (
	"fmt"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/number"
	"github.com/ethereum/go-ethereum/eth"
	"github.com/ethereum/go-ethereum/xeth"
)

// Wallet API
type WalletApi struct {
	Xeth     *xeth.XEth
	Ethereum *eth.Ethereum
}

// create new wallet API instance
func NewWalletApi(eth *eth.Ethereum) *WalletApi {
	xeth := xeth.New(eth, nil)
	return &WalletApi{xeth, eth}
}

func (self *WalletApi) SendMoney(fromStr, toStr, valueStr string) (string, error) {

	nonceStr := ""
	gasStr := ""
	gasPriceStr := ""
	codeStr := ""

	return self.Xeth.Transact(fromStr, toStr, nonceStr, valueStr, gasStr, gasPriceStr, codeStr)
}

func (self *WalletApi) GetBalance(address string) (string, error) {

	self.Xeth.UpdateState()
	res := self.Xeth.BalanceAt(address)
	if res == "invalid address" {
		return "0", fmt.Errorf("address not found")
	}

	if res == "0x0" {
		return "0", nil
	}

	value := number.Big(0).SetBytes(common.FromHex(res))
	return fmt.Sprintf("%d", value.Uint64()), nil
}